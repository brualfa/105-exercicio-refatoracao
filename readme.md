# Exercício de Orientação a Objetos - Geometria

1- Crie um sistema que permite a criação de formas geométricas e o cálculo de suas áreas. O sistema deve atender os seguintes requisitos:

- Deve se ter como formas obrigatórias o círculo, o triângulo e o retângulo.
- Para criar uma forma, basta que seja informado o tamanho de seus lados.
- Após criada uma forma, não é possível alterar o tamanho de seus lados.
- O sistema deve informar a área de qualquer forma já criada.
- No caso do triângulo, deve-se verificar se os lados informados geram um triângulo válido. Caso contrário, deve-se lançar uma exceção.
- O usuário pode informar um quantidade qualquer de lados pelo terminal e obter a área de uma forma baseada na quantidade de lados informada.
- Caso o usuário informe mais de 3 lados, deve-se lançar uma exceção.